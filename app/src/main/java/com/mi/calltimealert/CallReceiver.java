package com.mi.calltimealert;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.telephony.TelephonyManager;
import com.mi.calltimealert.util.L;

public class CallReceiver extends BroadcastReceiver {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    private static final long ONE_SECOND = 1000;
    private static final long TEN_SECONDS = 10 * ONE_SECOND;
    private static final long ONE_MINUTE = 60 * ONE_SECOND;
    private static final long TWO_MINUTES = 2 * 60 * ONE_SECOND;

    /************************************
     * PRIVATE STATIC FIELDS
     ************************************/
    private static boolean itsOn = false;
    private static boolean itsInComing = false;
    private static Context context;
    private static CountDownTimer countDownTimer1;
    private static CountDownTimer countDownTimer2 = new CountDownTimer(ONE_MINUTE, ONE_MINUTE) {

        @Override
        public void onTick(long millisUntilFinished) {
            L.i("onTick", millisUntilFinished);
            if (itsOn) {
                CallWidgetProvider.startPlay(context);
            }
        }

        @Override
        public void onFinish() {
            L.i("onFinish");
            countDownTimer3.start();
        }

    };
    private static CountDownTimer countDownTimer3 = new CountDownTimer(TWO_MINUTES, TWO_MINUTES) {
        @Override
        public void onTick(long millisUntilFinished) {
            L.i("onTick", millisUntilFinished);
            if (itsOn) {
                CallWidgetProvider.startPlay(context);
            }
        }

        @Override
        public void onFinish() {
            L.i("onFinish");
            countDownTimer3.start();
        }
    };

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static void startListen() {
        L.i("startListen");
        itsOn = true;
    }

    public static void stopListen() {
        L.i("stopListen");
        itsOn = false;
        CallWidgetProvider.pause();
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public void onReceive(Context mContext, Intent intent) {
        L.i("onReceive");

        context = mContext;

        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            L.i("onReceive", "ACTION_NEW_OUTGOING_CALL");
        }

        /*switch (intent.getIntExtra(TelephonyManager.EXTRA_FOREGROUND_CALL_STATE, -2) {
            case PreciseCallState.PRECISE_CALL_STATE_IDLE:
                System.out.println("CallReceiver.onReceive : " + "ACTION_NEW_OUTGOING_CALL");
                break;
            case PreciseCallState.PRECISE_CALL_STATE_DIALING:
                Log.d(This.LOG_TAG, "DIALING");
                break;
            case PreciseCallState.PRECISE_CALL_STATE_ALERTING:
                Log.d(This.LOG_TAG, "ALERTING");
                break;
            case PreciseCallState.PRECISE_CALL_STATE_ACTIVE:
                Log.d(This.LOG_TAG, "ACTIVE");
                break;
        }*/

        Bundle bundle = intent.getExtras();

        if (!bundle.containsKey(TelephonyManager.EXTRA_STATE)) {
            return;
        }

        String extraState = bundle.getString(TelephonyManager.EXTRA_STATE);

        if (extraState.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            L.i("onReceive - EXTRA_STATE_RINGING");
            // Phone number
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            itsInComing = true;
            // Ringing state
            // This code will execute when the phone has an incoming call
        } else if (extraState.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            L.i("onReceive - EXTRA_STATE_IDLE");
            // This code will execute when the call is answered or disconnected
            removeCountDown();
            CallWidgetProvider.pause();
            itsInComing = false;
        } else if (extraState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            L.i("onReceive - EXTRA_STATE_OFFHOOK");

            /**
             * Pavel:
             * we will play the sound for incoming and outgoing calls
             */
            /*if (itsInComing) {
                L.i("onReceive - EXTRA_STATE_OFFHOOK - return because its incoming");
                return;
            }*/

            if (countDownTimer1 == null) {
                countDownTimer1 = new CountDownTimer(ONE_MINUTE * 3, TEN_SECONDS) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        L.i("onTick", millisUntilFinished, itsOn);
                        // startPlay();
                        if (itsOn) {
                            CallWidgetProvider.startPlay(context);
                        }
                    }

                    @Override
                    public void onFinish() {
                        L.i("onFinish");
                        countDownTimer2.start();
                    }
                };
                L.i("onReceive - EXTRA_STATE_OFFHOOK - start the first countdown timer");
                countDownTimer1.start();
            }
        }

    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void removeCountDown() {
        L.i("removeCountDown");

        if (countDownTimer1 != null) {
            countDownTimer1.cancel();
        }

        if (countDownTimer2 != null) {
            countDownTimer2.cancel();
        }

        if (countDownTimer3 != null) {
            countDownTimer3.cancel();
        }

        countDownTimer1 = null;

    }

}