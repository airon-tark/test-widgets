package com.mi.calltimealert.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mi.calltimealert.R;
import com.mi.calltimealert.util.L;

/**
 * User: Pavel
 * Date: 05.11.2015
 * Time: 10:29
 */
public class RadioItemView extends RelativeLayout implements Checkable, View.OnClickListener {

    /*************************************
     * PRIVATE FIELDS
     *************************************/
    private boolean mChecked;
    private OnTurnOnListener mListener;
    private TextView mTitle;
    private ImageView mIcon;
    private boolean mToggleable;

    /*************************************
     * PUBLIC METHODS
     *************************************/
    public RadioItemView(Context context) {
        super(context);
        init(context);
    }

    public RadioItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RadioItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public RadioItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(true);
        if (mListener != null) {
            mListener.onTurnOn(this);
        }

    }

    @Override
    public void onClick(View v) {
        toggle();
    }

    @Override
    public void setChecked(boolean isChecked) {
        mChecked = isChecked;
        mIcon.setVisibility(mChecked ? VISIBLE : GONE);
    }

    public void setOnTurnOnListener(OnTurnOnListener listener) {
        mListener = listener;
    }

    public void setText(String text) {
        L.i("setText - " + text);
        mTitle.setText(text);
    }

    public void setText(int textResId) {
        L.i("setText - " + textResId);
        mTitle.setText(textResId);
    }

    public void setTextColor(int color) {
        L.i("setTextColor - " + color);
        mTitle.setTextColor(color);
    }

    /*************************************
     * PRIVATE METHODS
     *************************************/
    private void init(Context context) {
        L.i("init");
        inflate(context, R.layout.view_list_item, this);
        mTitle = (TextView) findViewById(R.id.title);
        mIcon = (ImageView) findViewById(R.id.icon);
        setClickable(true);
        setOnClickListener(this);
    }

    private void init(Context context, AttributeSet attributes) {
        L.i("init");
        init(context);

        TypedArray styledAttributes = getContext().obtainStyledAttributes(attributes, R.styleable.RadioItemView);
        int attrsCount = styledAttributes.getIndexCount();

        for (int i = 0; i < attrsCount; i++) {
            int styledAttributesIndex = styledAttributes.getIndex(i);

            switch (styledAttributesIndex) {
                case R.styleable.RadioItemView_itemTitle:
                    mTitle.setText(styledAttributes.getString(styledAttributesIndex));
                    break;
                case R.styleable.RadioItemView_itemChecked:
                    setChecked(styledAttributes.getBoolean(styledAttributesIndex, false));
                    break;
                case R.styleable.RadioItemView_itemIconColor:
                    mIcon.setColorFilter(styledAttributes.getColor(styledAttributesIndex, 0));
                    break;
                case R.styleable.RadioItemView_itemToggleable:
                    mToggleable = styledAttributes.getBoolean(styledAttributesIndex, true);
                    break;
            }
        }

        styledAttributes.recycle();
    }

    /*************************************
     * PUBLIC INTERFACES
     *************************************/
    public interface OnTurnOnListener {

        void onTurnOn(RadioItemView radioItemView);

    }

}
