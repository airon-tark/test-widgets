package com.mi.calltimealert.managers;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.widget.Toast;
import com.mi.calltimealert.App;
import com.mi.calltimealert.R;
import com.mi.calltimealert.util.L;

/**
 * User: Pavel
 * Date: 11.12.2015
 * Time: 17:44
 */
public class SoundPoolPlayer implements SoundPool.OnLoadCompleteListener {

    /*************************************
     * PUBLIC STATIC CONSTANTS
     *************************************/
    public static final int MAX_STREAMS = 5;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private SoundPool mSoundPool;
    private int mFileId;
    private boolean mReady = false;

    /************************************
     * PUBLIC METHODS
     ************************************/
    public SoundPoolPlayer(Context context, int fileResId) {
        mSoundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
        mSoundPool.setOnLoadCompleteListener(this);

        //try {
            //mFileId = mSoundPool.load(context.getAssets().openFd(fileName), 1);
            mFileId = mSoundPool.load(context, fileResId, 1);
        /*} catch (IOException e) {
            L.i("init - Exception - " + Log.getStackTraceString(e));
        }*/
        L.i("init - id", mFileId);
    }

    public void play() {
        L.i("play");

        if (mSoundPool == null) {
            showToast(R.string.error_sound_pool_is_null);
            return;
        }

        if (!mReady) {
            showToast(R.string.error_sound_pool_is_not_ready);
            return;
        }

        mSoundPool.play(mFileId, 1, 1, 0, 0, 1);
    }

    @Override
    public void onLoadComplete(SoundPool soundPool, int i, int i1) {
        mReady = true;
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void showToast(int StringResId) {
        L.i("showToast");
        Toast.makeText(
                App.getContext(),
                App.getContext().getString(R.string.error_sound_pool_is_null),
                Toast.LENGTH_LONG
        ).show();


    }
}
