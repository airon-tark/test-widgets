package com.mi.calltimealert.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.mi.calltimealert.CallWidgetProvider;
import com.mi.calltimealert.R;
import com.mi.calltimealert.util.L;
import com.mi.calltimealert.views.RadioItemView;

/**
 * User: Pavel
 * Date: 20.07.2016
 * Time: 15:04
 */
public class MainActivity extends AppCompatActivity implements RadioItemView.OnTurnOnListener {

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.media_player)
    RadioItemView mMediaPlayerItem;
    @BindView(R.id.sound_pool)
    RadioItemView mSoundPoolItem;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        L.i("onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mMediaPlayerItem.setOnTurnOnListener(this);
        mSoundPoolItem.setOnTurnOnListener(this);

        switch (CallWidgetProvider.getType()) {
            case CallWidgetProvider.Type.MEDIA_PLAYER:
                mMediaPlayerItem.setChecked(true);
                mSoundPoolItem.setChecked(false);
                break;
            case CallWidgetProvider.Type.SOUND_POOL:
                mMediaPlayerItem.setChecked(false);
                mSoundPoolItem.setChecked(true);
                break;
        }

    }

    @Override
    public void onTurnOn(RadioItemView radioItemView) {
        L.i("onTurnOn");
        switch (radioItemView.getId()) {
            case R.id.media_player:
                CallWidgetProvider.setType(CallWidgetProvider.Type.MEDIA_PLAYER);
                mSoundPoolItem.setChecked(false);
                break;
            case R.id.sound_pool:
                CallWidgetProvider.setType(CallWidgetProvider.Type.SOUND_POOL);
                mMediaPlayerItem.setChecked(false);
                break;
        }
    }

}
