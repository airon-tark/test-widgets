package com.mi.calltimealert;

import android.app.Application;
import android.content.Context;
import com.backendless.Backendless;
import com.mi.calltimealert.config.MyBackendless;
import com.mi.calltimealert.util.L;

/**
 * User: Pavel
 * Date: 13.07.2016
 * Time: 19:31
 */
public class App extends Application {

    private static App sInstance;

    @Override
    public void onCreate() {
        L.i("onCreate");
        super.onCreate();
        L.plant(new L.DebugTree());
        Backendless.initApp(this, MyBackendless.APP_ID, MyBackendless.SECRET_KEY, "v1");
        sInstance = this;
        CallWidgetProvider.initSoundPoolPlayer(sInstance);
    }

    public static Context getContext() {
        return sInstance;
    }

}
