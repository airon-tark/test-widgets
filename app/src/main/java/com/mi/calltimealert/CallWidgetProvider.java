package com.mi.calltimealert;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.backendless.Backendless;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.messaging.Message;
import com.backendless.messaging.MessageStatus;
import com.mi.calltimealert.managers.SoundPoolPlayer;
import com.mi.calltimealert.util.L;

import java.util.List;

/**
 * Implementation of App Widget functionality.
 */
public class CallWidgetProvider extends AppWidgetProvider {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static final String PREF_KEY = "TOGGLE_ON_OFF";
    public static final String PREF_ON_OFF_KEY = "PREF_ON_OFF";
    public static final int DEFAULT_TYPE = Type.MEDIA_PLAYER;

    /************************************
     * PUBLIC STATIC FIELDS
     ************************************/
    private static boolean doNotChangeState;
    private static boolean sSubscribed = false;
    private static MediaPlayer mediaPlayer;
    private static SoundPoolPlayer sSoundPoolPlayer;
    private static int[] sAppWidgetIds;
    private static int sType;
    private static boolean mUpdating = false;
    private static Toast mUpdatingToast;

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static void startPlay(Context context) {
        L.i("startPlay");
        switch (sType) {
            case Type.MEDIA_PLAYER:
                if (!getMediaPlayer(context).isPlaying()) {

                    // set volume to maximum
                    AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                    am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);

                    // force using the speaker phone
                    am.setMode(AudioManager.MODE_IN_CALL);
                    am.setSpeakerphoneOn(true);

                    L.i("startPlay - really");
                    getMediaPlayer(context).setVolume(1, 1);
                    getMediaPlayer(context).start();

                }
                break;
            case Type.SOUND_POOL:
                getSoundPoolPlayer(context).play();
                break;
        }
    }

    public static void stopPlay() {
        L.i("stopPlay");
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    public static void pause() {
        L.i("pause");
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }

    public static void setType(int type) {
        L.i("setType", type);
        sType = type;
    }

    public static int getType() {
        return sType;
    }

    public static void initSoundPoolPlayer(Context context) {
        L.i("initSoundPoolPlayer");
        sSoundPoolPlayer = getSoundPoolPlayer(context);
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        L.i("onUpdate", doNotChangeState);

        if (mUpdating){
            if (mUpdatingToast == null) {
                mUpdatingToast = Toast.makeText(context, "Widget is updating, please wait", Toast.LENGTH_SHORT);
            }
            //mUpdatingToast.cancel();
            mUpdatingToast.show();
            return;
        }

        mUpdating = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mUpdating = false;
            }
        }, 3000);

        sAppWidgetIds = appWidgetIds;

        if (doNotChangeState) {
            doNotChangeState = false;
        } else {
            toggleState(context, appWidgetManager, appWidgetIds);
            sendMessageToBackend(getState(context));
        }

        /**
         * Pavel:
         * Here we are subscribing first time to the messages from the server.
         * TODO:
         * we also will need to handle case when the widget is deleted from the
         * desktop. Because in that case maybe we will need to subscribe again.
         */
        if (!sSubscribed) {
            subscribeToMessages();
            sSubscribed = true;
        }
    }

    @Override
    public void onEnabled(Context context) {
        L.i("onEnabled");
        // Enter relevant functionality for when the first widget is created
        /**
         * Pavel:
         * I am not very sure about this flag, why we need it.
         * It created some bugs for me, so I just remove it for the moment.
         * All works good without it.
         */
        //doNotChangeState = true;
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
        L.i("onDisabled");
    }

    /************************************
     * PRIVATE STATIC METHODS
     ************************************/
    private void updateWidgetsUi(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        L.i("updateWidgetsUi");

        // update the interface
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.call_widget_provider);
        if (getState(context)) {
            views.setTextViewText(R.id.appwidget_toggle, context.getString(R.string.widget_on));
            views.setImageViewResource(R.id.icon, R.drawable.ic_on);
            views.setInt(R.id.root, "setBackgroundResource", R.drawable.bg_widget_on);
        } else {
            views.setTextViewText(R.id.appwidget_toggle, context.getString(R.string.widget_off));
            views.setImageViewResource(R.id.icon, R.drawable.ic_off);
            views.setInt(R.id.root, "setBackgroundResource", R.drawable.bg_widget_off);
        }

        // handle clicks
        Intent intent = new Intent(context, CallWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.root, pendingIntent);

        // update the widget
        appWidgetManager.updateAppWidget(appWidgetIds, views);

    }

    private void subscribeToMessages() {
        L.i("subscribeToMessages");

        AsyncCallback<List<Message>> messagesCallback = new AsyncCallback<List<Message>>() {

            public void handleResponse(List<Message> response) {
                L.i("subscribeToMessages - handleResponse", response.get(0).getData());
                boolean newState = (boolean) response.get(0).getData();
                if (getState(App.getContext()) != newState) {
                    toggleState(App.getContext(), AppWidgetManager.getInstance(App.getContext()), sAppWidgetIds);
                }
            }

            public void handleFault(BackendlessFault fault) {
                L.i("subscribeToMessages - handleFault", fault.getMessage());
            }

        };

        AsyncCallback<Subscription> subscriptionCallback = new AsyncCallback<Subscription>() {

            public void handleResponse(Subscription response) {
                L.i("handleResponse", response.getSubscriptionId());
                sSubscribed = true;
            }

            public void handleFault(BackendlessFault fault) {
                L.i("handleFault", fault.getMessage());
                sSubscribed = false;
            }

        };

        Backendless.Messaging.subscribe(messagesCallback, subscriptionCallback);

    }

    private void toggleState(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        L.i("toggleState");

        boolean state = !getState(context);
        saveState(context, state);

        if (state) {
            CallReceiver.startListen();
        } else {
            CallReceiver.stopListen();
        }

        updateWidgetsUi(context, appWidgetManager, appWidgetIds);

    }

    private boolean getState(Context context) {
        L.i("getState");
        return context
                .getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getBoolean(PREF_ON_OFF_KEY, false);
    }

    private void saveState(Context context, boolean state) {
        context
                .getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(PREF_ON_OFF_KEY, state)
                .apply();
    }

    private void sendMessageToBackend(Boolean state) {
        L.i("sendMessageToBackend", state);
        //MessageStatus status = Backendless.Messaging.publish(MyBackendless.CHANNEL_NAME, state);
        Backendless.Messaging.publish(state, new AsyncCallback<MessageStatus>() {
            @Override
            public void handleResponse(MessageStatus messageStatus) {
                L.i("sendMessageToBackend - handleResponse", messageStatus.getStatus());
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                L.i("sendMessageToBackend - handleFault", backendlessFault.getMessage());
            }
        });
    }

    private static MediaPlayer getMediaPlayer(Context context) {
        L.i("getMediaPlayer");
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(context, R.raw.audio_test_new);
        }
        return mediaPlayer;
    }

    private static SoundPoolPlayer getSoundPoolPlayer(Context context) {
        L.i("getSoundPoolPlayer");
        if (sSoundPoolPlayer == null) {
            sSoundPoolPlayer = new SoundPoolPlayer(context, R.raw.audio_test_new);
        }
        return sSoundPoolPlayer;
    }

    /************************************
     * PUBLIC INTERFACES
     ************************************/
    public interface Type {

        int MEDIA_PLAYER = 0;
        int SOUND_POOL = 1;

    }

}

